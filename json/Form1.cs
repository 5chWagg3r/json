﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;
using System.IO;

namespace json
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            string folder = AppDomain.CurrentDomain.BaseDirectory + "../../AsmenuDuomenys/";
            InitializeComponent();
            
            var asmuo = new Asmuo()
            {
                Vardas = "Jonas",
                Pavarde = "Jonaitis",
                GimimoData = new DateTime(1985, 12, 11),
                Ugis = 183
                
            };
            var adresas = new Asmuo.Adresas()
            {
                Miestas = "Vilnius",
                Gatve = "Fabijoniskes",
                NamoNr = 47
            };

            var json = JsonConvert.SerializeObject(asmuo) + JsonConvert.SerializeObject(adresas);
            
            if (!Directory.Exists(folder))
            {
                Directory.CreateDirectory(folder);
            }

            var filename = $"{asmuo.Vardas}_{asmuo.Pavarde}_duomenys.json";
            File.WriteAllText(folder + filename, json);

        }
        
        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
    public class Asmuo
    {
        public string Vardas { get; set; }

        public string Pavarde { get; set; }

        public DateTime GimimoData { get; set; }

        public double? Atlyginimas { get; set; }

        public int Ugis { get; set; }

        public class Adresas
        {
            public string Miestas { get; set; }

            public string Gatve { get; set; }

            public int NamoNr { get; set; }
        }
        
    }
}
